Install_ceph_rhel_82

Install ceph rhel 8.2 && rbd client on rhel 7.8


Ceph cluster состоит из 3 машин:

ceph-id01 адрес 10.1.6.65
ceph-id02 адрес 10.1.6.66
ceph-id03 адрес 10.1.6.67

Необходимо обновить все пакеты на всех нодах:
# yum update -y

Необходимо установить podman и python36:
# yum install podman python36 -y

Далее необходимо настроить синхронизацию времени и включаем его:
# dnf install chrony -y
# systemctl restart chronyd
# systemctl enable chronyd

Обновляем файл /etc/hosts если машины не добавлены в DNS снова на всех нодах:

10.1.6.65   sehd-ceph-id01
10.1.6.66   sehd-ceph-id02
10.1.6.67   sehd-ceph-id03

Для продолжения работы необходимо знать или поменять пароль от пользователя root на всех нодах:
# passwd

Небходимо сгенерировать ssh ключ для польвателя root и разложить его по остальным нодам:
# ssh-keygen
# ssh-copy-id -f -i /etc/ceph/ceph.pub root@sehd-ceph-id01
# ssh-copy-id -f -i /etc/ceph/ceph.pub root@sehd-ceph-id02
# ssh-copy-id -f -i /etc/ceph/ceph.pub root@sehd-ceph-id03

На все три ноды необходимо проинсталлировать репозиторий с ceph:
# curl --silent --remote-name --location https://github.com/ceph/ceph/raw/octopus/src/cephadm/cephadm
# chmod +x cephadm
# ./cephadm add-repo --release pacific # на момент написания статьи это latest версия

Устанавливаем cephadm
# dnf install cephadm

Далее для работы с ceph необходимо добавить строку в файл ~/.bashrc
# echo "alias ceph='cephadm shell -- ceph'" >> /root/.bashrc
# source /root/.bashrc

Инициализируем ceph cluster на первой ноде sehd-ceph-id01 (он же менеджер)
# cephadm bootstrap --mon-ip 10.1.6.65

Теперь необходимо добавить хосты в кластер:
# ceph orch host add sehd-ceph-id02 10.1.6.66
# ceph orch host add sehd-ceph-id03 10.1.6.67

Просмотреть хосты в кластере после добавления хостов:
# ceph orch host ls

Посмотреть доступные диски на нодах:
# ceph orch device ls

Добавляем диски с хостов в и запускаем демоны OSD:
# ceph orch daemon add osd sehd-ceph-id01:/dev/sdc
# ceph orch daemon add osd sehd-ceph-id02:/dev/sdc
# ceph orch daemon add osd sehd-ceph-id03:/dev/sdc

Посмотреть OSD в кластере:
# ceph osd tree

Создание пула:
# ceph osd pool create rbdpool 32

Включение пула в режим rbd:
# ceph osd pool application enable rbdpool rbd

Переключение пула в режим записи только в две реплики для ускорения работы:
# ceph osd pool set rbdpool min_size 2

Создание диска с имененм docker-image в пуле rbdpool:
# rbd create rbdpool/docker-image --size 50G

Создание пользователя и выдача прав:
# ceph auth get-or-create client.rbd mgr 'allow r' mon 'allow r' osd 'allow rwx pool=rbdpool'

Копирование ключа на клиента:
# ceph auth get-or-create client.rbd | ssh root@10.1.6.70 tee /etc/ceph/ceph.client.rbd.keyring

Создание ключа авторизации:
# ceph-authtool -C -n client.foo --gen-key keyring --mode 0644

Посмотреть статус кластера ceph:
# ceph -s 
или
# ceph status

Посмотреть информацию о raw storage и pools:
# ceph df



Обязательное отключение функций для клиентов на rhel 7.8 так как ядро не поддерживает эти функции:
# rbd feature disable rbdpool/docker-image object-map deep-flatten fast-diff

/Устароело
/Для установки дашборда необходимо на первой ноде выполнить команду:
/# yum install ceph-mgr-dashboard -y


Далее все команды выполняются на клиенте.

Установка клиента ceph на хост для подключения rbd диска:
# yum install -y ceph-common

Копируем файл /etc/ceph/ceph.conf и ключ на клиента:
# scp root@10.1.6.65:/etc/ceph/ceph.conf /etc/ceph.conf
# scp root@10.1.6.65:/etc/ceph/ceph.client.admin.keyring /etc/ceph/ceph.client.admin.keyring

Подключение диска rbd на клиенте (RHEL 7.8)
# rbd map --pool rbdpool docker-image --id admin
